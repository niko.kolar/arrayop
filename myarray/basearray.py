from typing import Tuple
from typing import List
from typing import Union


class BaseArray:
    """
    Razred BaseArray, za učinkovito hranjenje in dostopanje do večdimenzionalnih podatkov. Vsi podatki so istega
    številskega tipa: int ali float.

    Pri ustvarjanju podamo obliko-dimenzije tabele. Podamo lahko tudi tip hranjenih podatkov (privzeto je int) in
    podatke za inicializacijo (privzeto vse 0).

    Primeri ustvarjanja:
    a = BaseArray((10,)) # tabela 10 vrednosti, tipa int, vse 0
    a = BaseArray((3, 3), dtype=float) # tabela 9 vrednost v 3x3 matriki, tipa float, vse 0.0
    a = BaseArray((2, 3), dtype=float, dtype=float, data=(1., 2., 1., 2., 1., 2.)) # tabela 6 vrednost v 2x3 matriki,
                                                                               #tipa float, vrednosti 1. in 2.
    a = BaseArray((2, 3, 4, 5), data=tuple(range(120))) # tabela dimenzij 2x3x4x5, tipa int, vrednosti od 0 do 120


    Do podatkov dostopamo z operatorjem []. Pri tem moramo paziti, da uporabimo indekse, ki so znotraj oblike tabele.
    Začnemo z indeksom 0, zadnji indeks pa je dolžina dimenzije -1.
     Primeri dostopanja :

    a = BaseArray((2, 3, 4, 5))
    a[0, 1, 2, 1] = 10 # nastavimo vrednost 10 na mesto 0, 1, 2, 1
    v = a[0, 0, 0, 0] # preberemo vrednost na mestu 0, 0, 0 ,0

    a[0, 3, 0, 0] # neveljaven indeks, ker je vrednost 3 enaka ali večja od dolžine dimenzije

    Velikost tabele lahko preberemo z lastnostjo 'shape', podatkovni tip pa z 'dtype'. Primer:

    a.shape # to vrne vrednost  (2, 3, 4, 5)
    a.dtype # vrne tip int

    Čez tabelo lahko iteriramo s for zanko, lahko uporabimo operatorje/ukaze 'reversed' in 'in'.

    a = BaseArray((4,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
    for v in a: # for zanka izpiše vrednost od 1 do 8
        print(v)
    for v in reversed(a): # for zanka izpiše vrednost od 8 do 1
        print(v)

    2 in a # vrne True
    -1 in a # vrne False
    """
    def __init__(self, shape: Tuple[int], dtype: type=None, data: Union[Tuple, List]=None):
        """
        Create an initialize a multidimensional myarray of a selected data type.
        Init the myarray to 0.

        :param shape: tuple or list of integers, shape of constructed myarray
        :param dtype: type of data stored in myarray, float or int
        :param data: list of tuple of data values, must contain consistent types and
                     have a length that matches the shape.
        """

        if not isinstance(shape, (tuple, list)):
            raise Exception(f'shape {shape:} is not a tuple or list')
        for v in shape:
            if not isinstance(v, int):
                raise Exception(f'shape {shape:} contains a non integer {v:}')

        self.__shape = (*shape,)

        self.__steps = _shape_to_steps(shape)

        if dtype is None:
            dtype = float
        if dtype not in [int, float]:
            raise Exception('Type must by int or float.')
        self.__dtype = dtype

        n = 1
        for s in self.__shape:
            n *= s

        if data is None:
            if dtype == int:
                self.__data = [0]*n
            elif dtype == float:
                self.__data = [0.0]*n
        else:
            if len(data) != n:
                raise Exception(f'shape {shape:} does not match provided data length {len(data):}')

            for d in data:
                if not isinstance(d, (int, float)):
                    raise Exception(f'provided data does not have a consistent type')
            self.__data = [d for d in data]

    @property
    def shape(self):
        """
        :return: a tuple representing the shape of the myarray.
        """
        return self.__shape

    @property
    def data(self):
        return self.__data

    @property
    def dtype(self):
        """
        :return: the type stored in the myarray.
        """
        return self.__dtype

    def __test_indice_in_range(self, ind):
        """
        Test if the given indice is in within the range of this myarray.
        :param ind: the indices used
        :return: True if indices are valid, False otherwise.
        """
        for ax in range(len(self.__shape)):
            if ind[ax] < 0 or ind[ax] >= self.shape[ax]:
                raise Exception('indice {:}, axis {:d} out of bounds (0, {:})'.format(ind, ax, self.__shape[ax]))

    def __getitem__(self, indice):
        """
        Return a value from this myarray.
        :param indice: indice to this myarray, integer or tuple
        :return: value at indice
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        return self.__data[key_lin]

    def __setitem__(self, indice, value):
        """
        Set a value in this myarray.
        :param indice: valued indice to a position, integer of tuple
        :param value: value to set
        :return: does not return
        """
        if not type(indice) is tuple:
            indice = (indice,)

        if not _is_valid_indice(indice):
            raise Exception(f'{indice} is not a valid indice')

        if len(indice) != len(self.__shape):
            raise Exception(f'indice {indice} is not valid for this myarray')

        self.__test_indice_in_range(indice)
        key_lin = _multi_to_lin_ind(indice, self.__steps)
        self.__data[key_lin] = value

    def __iter__(self):
        for v in self.__data:
            yield v

    def __reversed__(self):
        for v in reversed(self.__data):
            yield v

    def __contains__(self, item):
        return item in self.__data


def _multi_ind_iterator(multi_inds, shape):
    inds = multi_inds[0]
    s = shape[0]

    if type(inds) is slice:
        inds_itt = range(s)[inds]
    elif type(inds) is int:
        inds_itt = (inds,)

    for ind in inds_itt:
        if len(shape) > 1:
            for ind_tail in _multi_ind_iterator(multi_inds[1:], shape[1:]):
                yield (ind,)+ind_tail
        else:
            yield (ind,)


def _shape_to_steps(shape):
    dim_steps_rev = []
    s = 1
    for d in reversed(shape):
        dim_steps_rev.append(s)
        s *= d
    steps = tuple(reversed(dim_steps_rev))
    return steps


def _multi_to_lin_ind(inds, steps):
    i = 0
    for n, s in enumerate(steps):
        i += inds[n]*s
    return i


def _is_valid_indice(indice):
    if isinstance(indice, tuple):
        if len(indice) == 0:
            return False
        for i in indice:
            if not isinstance(i, int):
                return False
    elif not isinstance(indice, int):
        return False
    return True

def izpis3Dmatrike(matrika):
    vrni = ""
    print("izpis 3D matrike: ")
    sirina = matrika.shape[0]
    dolzina = matrika.shape[1]
    for i in range(matrika.shape[2]):
        print("izpisujem " + str(i + 1) + " rezino 3D matrike")
        matrika2D = BaseArray((sirina, dolzina))
        for j in range(sirina):
            for k in range(dolzina):
                matrika2D[j, k] = matrika[j, k, i]
        vrni += izpis2DMatrike(matrika2D)
    return vrni

def izpis2DMatrike(matrika):
    vrni = ""
    print("izpis 2D matrike: ")
    najvecjeStevilo = -9999999
    najmanjseStevilo = 9999999

    sirina = matrika.shape[0]
    dolzina = matrika.shape[1]

    for i in range(sirina):
        for j in range(dolzina):
            if matrika[i, j] > najvecjeStevilo:
                najvecjeStevilo = matrika[i, j]
            if matrika[i, j] < najmanjseStevilo:
                najmanjseStevilo = matrika[i, j]

    indent=0
    dolzinaV = str(najvecjeStevilo).__len__()
    dolzinaM = str(najmanjseStevilo).__len__()

    if dolzinaV > dolzinaM:
        indent = dolzinaV
    else:
        indent = dolzinaM

    indent+=1

    for i in range(sirina):
        for j in range(dolzina):
            dolzinastevilke = str(matrika[i,j]).__len__()
            for k in range(indent - dolzinastevilke):
                print(' ', end='')
                vrni += ' '
            print(matrika[i,j], end='')
            vrni += str(matrika[i,j])
        print("")
        vrni += "\n"
    return vrni

def izpis1DMatrike(matrika):
    print("izpis 1D matrike: ")
    vrni = ""
    for i in range(matrika.shape[0]):
        print(str(matrika[i]) + " ", end='')
        vrni += str(matrika[i]) + " "
    return vrni

def izpisMatrike(matrika):
    ndim = matrika.shape.__len__()
    if ndim == 2:
        izpis2DMatrike(matrika)
    elif ndim == 3:
        izpis3Dmatrike(matrika)
    elif ndim == 1:
        izpis1DMatrike(matrika)

Matrika1D = BaseArray((8,), data=(1, 2, 3, 4, 5, 6, 7, 8))
"""
Matrika2D = BaseArray((2,2), data=(1, 2, 3, 4))
Matrika3D = BaseArray((2,2,2), data=(1, 2, 3, 4, 5, 6, 7, 8))
Matrika1D = BaseArray((8,), data=(1, 2, 3, 4, 5, 6, 7, 8))
izpisMatrike(Matrika2D)
print("")
izpisMatrike(Matrika3D)
print("")
izpisMatrike(Matrika1D)
"""

print("")
print("-------------sortiranje")
print("")



print("")
print("-------------iskanje")
print("")


def isci1D(matrika, vrednost):
    print("( ", end="")
    vrni = "( "
    for i in range(matrika.shape[0]):
        if matrika[i] == vrednost:
            print("("+str(i)+"),", end="")
            vrni += "("+str(i)+"),"
    print(" )", end="")
    vrni += " )"
    return vrni


def isci2D(matrika, vrednost):
    print("( ", end="")
    vrni = "( "
    for i in range(matrika.shape[0]):
        for j in range(matrika.shape[1]):
            if matrika[i,j] == vrednost:
                print("(" + str(i) + ", " + str(j)+"), ", end="")
                vrni += "(" + str(i) + ", " + str(j)+"), "
    print(" )", end="")
    vrni += " )"
    return vrni


def isci3D(matrika, vrednost):
    print("( ", end="")
    vrni = "( "
    for i in range(matrika.shape[0]):
        for j in range(matrika.shape[1]):
            for k in range(matrika.shape[2]):
                if matrika[i,j,k] == vrednost:
                    print("(" + str(i) + ", " + str(j)+ ", " + str(k) + "), ", end="")
                    vrni += "(" + str(i) + ", " + str(j)+ ", " + str(k) + "), "
    print(" )", end="")
    vrni += " )"
    return vrni


def iskanjeVMatriki(matrika,vrednost):
    ndim = matrika.shape.__len__()
    if ndim == 2:
        isci2D(matrika, vrednost)
    elif ndim == 3:
        isci3D(matrika, vrednost)
    elif ndim == 1:
        isci1D(matrika, vrednost)
iskanjeVMatriki(Matrika1D, 2)
"""
iskanjeVMatriki(Matrika1D, 2)
print("")
iskanjeVMatriki(Matrika2D, 3)
print("")
iskanjeVMatriki(Matrika3D, 7)
print("")
"""
print("")
print("-------------matematične operacije")
print("")

def sestevaj(matrika, skalarAliMatrika):
    if isinstance(skalarAliMatrika, int) or isinstance(skalarAliMatrika, float):
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] += skalarAliMatrika
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] += skalarAliMatrika
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] += skalarAliMatrika
    else:
        if len(matrika.shape) == len(skalarAliMatrika.shape):
            isteDimenzije = 1
            for i in range(len(matrika.shape)):
                if matrika.shape[i] != skalarAliMatrika.shape[i]:
                    print("napacne velikosti dimenzij")
                    return
        else:
            print("razlicno stevilo dimenzij")
            return
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] += skalarAliMatrika[i]
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] += skalarAliMatrika[i,j]
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] += skalarAliMatrika[i,j,k]


def odstevaj(matrika, skalarAliMatrika):
    if isinstance(skalarAliMatrika, int) or isinstance(skalarAliMatrika, float):
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] -= skalarAliMatrika
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] -= skalarAliMatrika
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] -= skalarAliMatrika
    else:
        if len(matrika.shape) == len(skalarAliMatrika.shape):
            isteDimenzije = 1
            for i in range(len(matrika.shape)):
                if matrika.shape[i] != skalarAliMatrika.shape[i]:
                    print("napacne velikosti dimenzij")
                    return
        else:
            print("razlicno stevilo dimenzij")
            return
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] -= skalarAliMatrika[i]
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] -= skalarAliMatrika[i,j]
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] -= skalarAliMatrika[i,j,k]


def mnozi(matrika, skalarAliMatrika):
    if isinstance(skalarAliMatrika, int) or isinstance(skalarAliMatrika, float):
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] *= skalarAliMatrika
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] *= skalarAliMatrika
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] *= skalarAliMatrika
    else:
        if matrika.shape[1] == skalarAliMatrika.shape[0]:
            rezultat = BaseArray((matrika.shape[0], skalarAliMatrika.shape[1]))
            for i in range(matrika.shape[0]):
                # iterate through columns of Y
                for j in range(skalarAliMatrika.shape[1]):
                    # iterate through rows of Y
                    for k in range(skalarAliMatrika.shape[0]):
                        rezultat[i, j] += (matrika[i, k] * skalarAliMatrika[k, j])
            return rezultat
        else:
            print("Matriki sta nepravilnih dimenzij")
            return "Matriki sta nepravilnih dimenzij"


def deli(matrika, skalarAliMatrika):
    if isinstance(skalarAliMatrika, int) or isinstance(skalarAliMatrika, float):
        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] /= skalarAliMatrika
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] /= skalarAliMatrika
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] /= skalarAliMatrika
    else:
        if len(matrika.shape) == len(skalarAliMatrika.shape):
            isteDimenzije=1
            for i in range(len(matrika.shape)):
                if matrika.shape[i] != skalarAliMatrika.shape[i]:
                    print("napacne velikosti dimenzij")
                    return
        else:
            print("razlicno stevilo dimenzij")
            return

        if len(matrika.shape) == 1:
            for i in range(matrika.shape[0]):
                matrika[i] /= skalarAliMatrika[i]
        elif len(matrika.shape) == 2:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    matrika[i,j] /= skalarAliMatrika[i,j]
        elif len(matrika.shape) == 3:
            for i in range(matrika.shape[0]):
                for j in range(matrika.shape[1]):
                    for k in range(matrika.shape[2]):
                        matrika[i, j, k] /= skalarAliMatrika[i,j,k]


from math import log


def logaritmiraj(matrika, osnova):
    if len(matrika.shape) == 1:
        for i in range(matrika.shape[0]):
            matrika[i] = log(matrika[i], osnova)
    elif len(matrika.shape) == 2:
        for i in range(matrika.shape[0]):
            for j in range(matrika.shape[1]):
                matrika[i,j] = log(matrika[i,j], osnova)
    elif len(matrika.shape) == 3:
        for i in range(matrika.shape[0]):
            for j in range(matrika.shape[1]):
                for k in range(matrika.shape[2]):
                    matrika[i, j, k] = log(matrika[i,j,k], osnova)


from math import pow


def potenciraj(matrika, eksponent):
    if len(matrika.shape) == 1:
        for i in range(matrika.shape[0]):
            matrika[i] = pow(matrika[i], eksponent)
    elif len(matrika.shape) == 2:
        for i in range(matrika.shape[0]):
            for j in range(matrika.shape[1]):
                matrika[i,j] = pow(matrika[i,j], eksponent)
    elif len(matrika.shape) == 3:
        for i in range(matrika.shape[0]):
            for j in range(matrika.shape[1]):
                for k in range(matrika.shape[2]):
                    matrika[i, j, k] = pow(matrika[i,j,k], eksponent)
"""
sestevaj(Matrika2D, Matrika2D)
izpisMatrike(Matrika2D)

#odstevaj(Matrika2D, Matrika2D)
#izpisMatrike(Matrika2D)

matrika = BaseArray((Matrika2D.shape[0], Matrika2D.shape[1]))
matrika = mnozi(Matrika2D, Matrika2D)
#izpisMatrike(Matrika2D)

#deli(Matrika2D, Matrika2D)
#izpisMatrike(Matrika2D)

#logaritmiraj(Matrika2D, 2)
#izpisMatrike(Matrika2D)


#sestevaj(Matrika2D, Matrika1D)
#potenciraj(Matrika2D,2)
izpisMatrike(matrika)
"""
